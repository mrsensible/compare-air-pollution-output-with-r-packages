options(scipen = 100, "rgdal_show_exportToProj4_warnings"="none")
library(tidyverse)
library(sf)
library(raster)
library(rgdal)
library(gstat)
library(moments)

load("../Data/pm10.RData")
stations <- read_sf("../Data/stations_10km.shp")
stations_df <- stations %>% filter (F.R == "Fixed")%>% st_set_geometry(NULL)
seoul <- read_sf("../Data/Seoul_City.shp") %>% as('Spatial') %>% fortify()
pm10.winter <- merge(pm10.win.bk, stations_df, by.x = c("Station.ID", "X", "Y"), by.y = c("Station", "X", "Y"))
coordinates(pm10.winter) <- ~X+Y
proj4string(pm10.winter) <- CRS("+init=epsg:5181")


mid.jan <- pm10.winter[23:42]

sk <- data.frame(Date = mid.jan@data %>% 
                   reshape2::melt(id.vars = NULL, variable.name = "Date", value.name = "pm10") %>% 
                   dplyr::select(1) %>% unique(),
                 mean = colMeans(mid.jan@data[1:20]) %>% round(2),
                 median = apply(mid.jan@data[1:20], 2, FUN = median) %>% round(2),
                 skewness = skewness(mid.jan@data[1:20]) %>% round(2),
                 kurtosis = kurtosis(mid.jan@data[1:20]) %>% round(2))


mid.jan@data %>% 
  reshape2::melt(id.vars = NULL, variable.name = "Date", value.name = "pm10") %>% 
  ggplot(aes(x= pm10, fill= Date)) +
  geom_histogram(binwidth=10, colour = "black")+
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("mean = " , mean)), hjust = -0.05, vjust = 1.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("median = " , median)), hjust = -0.05, vjust = 2.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("skewness = " , skewness)), hjust = -0.05, vjust = 3.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("kurtosis = " , kurtosis)), hjust = -0.05, vjust = 4.1, size = 3.5) +
  facet_wrap(~Date) +
  theme_bw() +
  theme(legend.position = "none",
        strip.text.x = element_text(size = 15)) -> hist_poll


ggsave("../Results/Hist/pm10_hist_01_S2.png", hist_poll, width = 12, height = 10)#, scale = 1.5)


##########
options(warn = -1) # don't print warnings
myVario <- list()
myList <- list()

for(i in 1:20){
  myVario[[length(myVario)+1]] <- variogram(mid.jan[[i]] ~ 1, mid.jan, cutoff = 30000, width = 3000)
  myList[[i]]  <- fit.variogram(myVario[[i]], 
                                vgm(psill = 15,
                                    nugget= 30,
                                    model = "Ste"),
                                fit.kappa = TRUE, fit.method = 6)
  }

myList[[1]]  <- fit.variogram(myVario[[1]], 
                              vgm(psill = 10,
                                  #range = 80000,
                                  nugget= 30,
                                  model = "Ste",
                                  kappa = 50),
                              fit.kappa = TRUE, fit.method = 1)


myList[[2]]  <- fit.variogram(myVario[[2]], 
                              vgm(psill = 10,
                                  #range = 80000,
                                  nugget= 40,
                                  model = "Ste",
                                  kappa = 50),
                              fit.kappa = TRUE, fit.method = 1)

myList[[4]]  <- fit.variogram(myVario[[4]], 
                              vgm(psill = 10,
                                  #range = 80000,
                                  nugget= 33,
                                  model = "Ste",
                                  kappa = 50),
                              fit.kappa = TRUE, fit.method = 1)


myList[[7]]  <- fit.variogram(myVario[[7]], 
                              vgm(psill = 100,
                                  range = 45000,
                                  nugget= 40,
                                  model = "Ste",
                                  kappa = 50),
                              fit.kappa = TRUE, fit.method = 6)



myList[[9]]  <- fit.variogram(myVario[[9]], 
                              vgm(psill = 100,
                                  range = 45000,
                                  nugget= 40,
                                  model = "Ste",
                                  kappa = 50),
                              fit.kappa = TRUE, fit.method = 6)

myList[[10]]  <- fit.variogram(myVario[[10]], 
                              vgm(psill = 100,
                                  range = 45000,
                                  nugget= 40,
                                  model = "Ste",
                                  kappa = 50),
                              fit.kappa = TRUE, fit.method = 6)

myList[[11]]  <- fit.variogram(myVario[[11]], 
                               vgm(psill = 10,
                                   range = 80000,
                                   nugget= 59,
                                   model = "Ste",
                                   kappa = 50),
                               fit.kappa = TRUE, fit.method = 6)

myList[[12]]  <- fit.variogram(myVario[[12]], 
                               vgm(psill = 100,
                                   range = 45000,
                                   nugget= 70,
                                   model = "Ste",
                                   kappa = 50),
                               fit.kappa = TRUE, fit.method = 6)

myList[[13]]  <- fit.variogram(myVario[[13]], 
                               vgm(psill = 150,
                                   range = 45000,
                                   nugget= 100,
                                   model = "Ste",
                                   kappa = 50),
                               fit.kappa = TRUE, fit.method = 6)



myList[[19]]  <- fit.variogram(myVario[[19]], 
                               vgm(psill = 10,
                                   range = 80000,
                                   nugget= 36,
                                   model = "Ste",
                                   kappa = 50),
                               fit.kappa = TRUE, fit.method = 6)

myList[[20]]  <- fit.variogram(myVario[[20]], 
                               vgm(psill = 6.5,
                                   range = 59000,
                                   nugget= 18,
                                   model = "Ste",
                                   kappa = 50))

library(gridExtra)
p01 <- plot(myVario[[1]],  myList[[1]],  main = "Jan 11st\nDay hours")
p02 <- plot(myVario[[2]],  myList[[2]],  main = "Jan 11st\nNight hours")
p03 <- plot(myVario[[3]],  myList[[3]],  main = "Jan 12th\nDay hours")
p04 <- plot(myVario[[4]],  myList[[4]],  main = "Jan 12th\nNight hours")
p05 <- plot(myVario[[5]],  myList[[5]],  main = "Jan 13th\nDay hours")
p06 <- plot(myVario[[6]],  myList[[6]],  main = "Jan 13th\nNight hours")
p07 <- plot(myVario[[7]],  myList[[7]],  main = "Jan 14th\nDay hours")
p08 <- plot(myVario[[8]],  myList[[8]],  main = "Jan 14th\nNight hours")
p09 <- plot(myVario[[9]],  myList[[9]],  main = "Jan 15th\nDay hours")
p10 <- plot(myVario[[10]], myList[[10]], main = "Jan 15th\nNight hours")
p11 <- plot(myVario[[11]], myList[[11]], main = "Jan 16th\nDay hours")
p12 <- plot(myVario[[12]], myList[[12]], main = "Jan 16th\nNight hours")
p13 <- plot(myVario[[13]], myList[[13]], main = "Jan 17th\nDay hours")
p14 <- plot(myVario[[14]], myList[[14]], main = "Jan 17th\nNight hours")
p15 <- plot(myVario[[15]], myList[[15]], main = "Jan 18th\nDay hours")
p16 <- plot(myVario[[16]], myList[[16]], main = "Jan 18th\nNight hours")
p17 <- plot(myVario[[17]], myList[[17]], main = "Jan 19th\nDay hours")
p18 <- plot(myVario[[18]], myList[[18]], main = "Jan 19th\nNight hours")
p19 <- plot(myVario[[19]], myList[[19]], main = "Jan 20th\nDay hours")
p20 <- plot(myVario[[20]], myList[[20]], main = "Jan 20th\nNight hours")


varplot <- grid.arrange(p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, 
                        p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, 
                        ncol = 6)

ggsave("../Results/Semivariogram/pm10_semvario_01_S2.png",varplot, width = 12, height = 10)#, scale = 1.5)


### Data Frame
seoul_grid <- data.frame(expand.grid(X = seq(min(pm10.winter$X), max(pm10.winter$X), length=200),
                                     Y = seq(min(pm10.winter$Y), max(pm10.winter$Y), length=200)))
coordinates(seoul_grid) <- ~X+Y
proj4string(seoul_grid) <- CRS("+init=epsg:5181")

#https://gis.stackexchange.com/questions/157279/saving-results-in-automap-r-package-for-time-series-data

##############
#--Kriging--##
##############
pred.model <- seoul_grid@coords
var.model <- seoul_grid@coords

for(i in 1:20) {
  kriging_new <- krige(mid.jan@data[,i]~ X + Y,
                       mid.jan, 
                       seoul_grid,
                       #nmin = 20,
                       #nmax = 50,
                       model = myList[[i]])
  kriging_new$var_model <- data.frame(kriging_new$var1.var)
  var.model <- cbind(var.model, kriging_new$var_model)
  xyz <- as.data.frame(kriging_new$var1.pred)
  colnames(xyz) <- colnames(mid.jan@data)[i]
  pred.model <- cbind(pred.model, xyz)
} 

##-- Add ColNames
colnames(pred.model) <- c("X", "Y", "jan11d", "jan11n", "jan12d", "jan12n","jan13d", "jan13n", "jan14d", "jan14n", "jan15d", "jan15n", "jan16d", "jan16n", "jan17d", "jan17n", "jan18d", "jan18n", "jan19d", "jan19n", "jan20d", "jan20n")

colnames(var.model) <- c("X", "Y", "jan11d", "jan11n", "jan12d", "jan12n","jan13d", "jan13n", "jan14d", "jan14n", "jan15d", "jan15n", "jan16d", "jan16n", "jan17d", "jan17n", "jan18d", "jan18n", "jan19d", "jan19n", "jan20d", "jan20n")

##-- Find Mean and variance
stat <- pred.model %>% dplyr::select(-c(X,Y)) %>% 
        gather(factor_key = T) %>% 
        group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
        rename(Hour = key)

statvar <- var.model %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), median = round(median(value),1), 
                              sd= round(sd(value),1), max = max(value),min = min(value)) %>% rename(Hour = key)

########
# RMSE #
########
pred <- pred.model
r.pred <- rasterFromXYZ(pred)
crs(r.pred) <- CRS('+init=epsg:5181')

obs <- as.data.frame(mid.jan) %>% dplyr::select(X,Y,everything())
pred.df <- data.frame(X = obs$X, Y = obs$Y)
RMSE <- data.frame(X = obs$X, Y = obs$Y)

# for loop
for(i in 1:20){
  pred.df[,i+2] <- extract(r.pred[[i]], mid.jan)
  RMSE[,i+2] <- sqrt(abs(pred.df[,i+2] - obs[,i+2]))^2
  
}

colnames(RMSE) <- colnames(obs)
RMSE %>% dplyr::select(-c(1:2)) %>% as.matrix() %>% mean()
stat$rmse <- RMSE %>% dplyr::select(-c(1:2)) %>% colMeans() %>% round(digits = 3)

RMSE %>% 
  filter_all(all_vars(. > 1)) %>% 
  left_join(stations_df, by = c("X", "Y")) %>% 
  dplyr::select(-c(Road_Dist, DEM)) -> count_RMSE_over1

count_RMSE_over1 %>% dim()

write.csv(count_RMSE_over1, "../Results/RMSE/pm10_kr_01_S2.csv", row.names = F)

#############
#- Plotting-#
#############

ras.krige.df <- pred.model %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "pm10") 

ras.krige.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_contour(aes(x = X, y = Y, z = pm10),bins = 30, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = stat, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) +
  geom_text(data = stat, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) +
  geom_text(data = stat, aes(-Inf, Inf, label = paste0("RMSE=" , rmse)), hjust = -.1, vjust = 1.2, size = 3.5) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> kriged # 1200 x 550 

# Export PNG
ggsave("../Results/Outcome/pm10_kriged_pred_01_S2.png", kriged, width = 11, height = 5, dpi = 100)



ras.var.df <- var.model %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "pm10") 

ras.var.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = pm10)) +
  scale_fill_distiller(palette = "BrBG", na.value = NA) + #, limits = c(0,12), breaks = c(0,3,6,9,12)) +
  geom_contour(aes(x = X, y = Y, z = pm10),bins = 30, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = statvar, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) +
  geom_text(data = statvar, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> kriged_stv 

# Export PNG
ggsave("../Results/Outcome/pm10_kriged_var_01_S2.png", kriged_stv, width = 11, height = 5, dpi = 100)



####################
#--Export Boxplot--#
####################
ras.krige.df$Month <- "Jan"
write.csv(ras.krige.df, "../Results/Boxplot/pm10_01_S2_kr.csv", row.names = F)


# convert to Raster Bricks
krige <- rasterFromXYZ(pred.model, 
                       crs="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
                       digits=5)
###################
#--Export Raster--#
###################
writeRaster(krige, filename="../Results/Mapchange/pm10_01_S2_kr.tif", format="GTiff", overwrite=TRUE)

ras.road <- raster("../Data/road_10km_re.tif")  # Import raster
res.mgcv <- resample(krige, ras.road, method = "bilinear") # resample 
res.mgcv <- merge(ras.road, res.mgcv) # merge

# assign road
road_01 = road_02 = road_03 = road_04 = road_05 = 
  road_06 = road_07 = road_08 = road_09 = road_10 =
  road_11 = road_12 = road_13 = road_14 = road_15 = 
  road_16 = road_17 = road_18 = road_19 = road_20 = ras.road

# stack raster and remove individual raster files
road.stack <- stack(road_01, road_02, road_03, road_04, road_05, 
                    road_06, road_07, road_08, road_09, road_10,
                    road_11, road_12, road_13, road_14, road_15, 
                    road_16, road_17, road_18, road_19, road_20
)
rm(road_01, road_02, road_03, road_04, road_05, 
   road_06, road_07, road_08, road_09, road_10,
   road_11, road_12, road_13, road_14, road_15, 
   road_16, road_17, road_18, road_19, road_20
)

# add road ratio values to GAM raster
ratio.mid.jan <- pm10.win.ratio[21:40,]


for(i in 1:20){
  #road.stack[[i]] <- road.stack[[i]] * ratio.pm10.sum$ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 1] <- ratio.mid.jan$Back.Road.Ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 2] <- ratio.mid.jan$Back.High.Ratio[i]
}

# add pm10 and road values
r.poll.rd <- overlay(res.mgcv, road.stack, fun = function(x,y){ifelse(y != 0, x*y, x)})
names(r.poll.rd) <- c("jan11d", "jan11n", "jan12d", "jan12n","jan13d", "jan13n", "jan14d", "jan14n", "jan15d", "jan15n", "jan16d", "jan16n", "jan17d", "jan17n", "jan18d", "jan18n", "jan19d", "jan19n", "jan20d", "jan20n")

writeRaster(r.poll.rd, filename="../Results/Mapchange/pm10_01_S2_kr_final.tif", format="GTiff", overwrite=TRUE)
#####################
#ras.mgcv.df <- as.data.frame(r.poll.rd, xy = TRUE) # easy way
# however, since we resampled and changed our data
# with different resolution imamges and extent, the easier way doesn't work
ras <- xyFromCell(r.poll.rd, 1:ncell(r.poll.rd))
krige.df <- as.data.frame(r.poll.rd)

##-- Find Mean and variance

ras.krige.stat <- data.frame(ras, krige.df)

stat1 <- ras.krige.stat %>% dplyr::select(-c(x,y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
  rename(Hour = key)

#####
ras.krige.df <- data.frame(ras, krige.df) %>% 
  reshape2::melt(id = c("x", "y"), variable.name = "Hour", value.name = "pm10") 

ras.krige.df %>% 
  ggplot() +
  geom_tile(aes(x = x, y = y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_text(data = stat1, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) + 
  geom_text(data = stat1, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) + 
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> final

# Export PNG
ggsave("../Results/Outcome_With_Road/pm10_kriged_final_01_S2.png", final, width = 11, height = 5, dpi = 100)



##############
#--Extract--##
##############
# Attributes of Monitoring stations
back_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Fixed") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

road_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Road") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))


# Spatial Points

back <- mid.jan

mid.jan.rd <- pm10.win.rd[,c(1:2, 25:44)]
coordinates(mid.jan.rd) <- ~X+Y
proj4string(mid.jan.rd) <- CRS("+init=epsg:5181")

road <- mid.jan.rd


colnames(back@data) <- c("jan11d", "jan11n", "jan12d", "jan12n","jan13d", "jan13n", "jan14d", "jan14n", "jan15d", "jan15n", "jan16d", "jan16n", "jan17d", "jan17n", "jan18d", "jan18n", "jan19d", "jan19n", "jan20d", "jan20n")

colnames(road@data) <- c("jan11d", "jan11n", "jan12d", "jan12n","jan13d", "jan13n", "jan14d", "jan14n", "jan15d", "jan15n", "jan16d", "jan16n", "jan17d", "jan17n", "jan18d", "jan18n", "jan19d", "jan19n", "jan20d", "jan20n")


# Silim Coordinates: 195793.5 442361.5
# Gwanakgu Office location: 193837.2 442737.9

###########
#--Final--#
###########
back_final <- back_df %>% filter(Station != 131233) %>% cbind(extract(r.poll.rd, back))
back_obs   <- back_df %>% filter(Station != 131233) %>% cbind(back@data)
back_pred  <- back_df %>% filter(Station != 131233) %>% cbind(extract(krige, back))
road_final <- road_df %>% cbind(extract(r.poll.rd, road))
road_obs   <- road_df %>% cbind(road@data)
road_pred  <- road_df %>% cbind(extract(krige, road))

##########
#--Plot--#
##########

plot_back_pred <- reshape2::melt(back_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_back_final <- reshape2::melt(back_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")

								 
plot_back_obs <- reshape2::melt(back_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_back_fin <- rbind(plot_back_pred, plot_back_final) %>% rbind(plot_back_obs)



##
plot_road_pred <- reshape2::melt(road_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_road_final <- reshape2::melt(road_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")
								 
plot_road_obs <- reshape2::melt(road_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_road_fin <- rbind(plot_road_pred, plot_road_final) %>% rbind(plot_road_obs)



# Export File
write.csv(plot_back_fin, "../Results/Validation/pm10_Jan_S2_kr.csv",row.names=FALSE)
write.csv(plot_road_fin, "../Results/Validation/pm10_rd_Jan_S2_kr.csv",row.names=FALSE)
