options(scipen = 100, "rgdal_show_exportToProj4_warnings"="none")
library(tidyverse)
library(sf)
library(mgcv)
library(raster)

load("../Data/pm10.RData")
stations <- read_sf("../Data/stations_10km.shp")
stations_df <- stations %>% filter (F.R == "Fixed")%>% st_set_geometry(NULL)
seoul <- read_sf("../Data/Seoul_City.shp") %>% as_Spatial() %>% fortify()
pm10.winter <- merge(pm10.win.bk, stations_df, by.x = c("Station.ID", "X", "Y"), by.y = c("Station", "X", "Y"))


#FEBRUARY GAM MODEL
feb11d <- gam(pm10_2_11_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb12d <- gam(pm10_2_12_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb13d <- gam(pm10_2_13_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb14d <- gam(pm10_2_14_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb15d <- gam(pm10_2_15_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb16d <- gam(pm10_2_16_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb17d <- gam(pm10_2_17_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb18d <- gam(pm10_2_18_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb19d <- gam(pm10_2_19_day ~ s(X, Y), data = pm10.winter, method = "ML")
feb20d <- gam(pm10_2_20_day ~ s(X, Y), data = pm10.winter, method = "ML")

feb11n <- gam(pm10_2_11_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb12n <- gam(pm10_2_12_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb13n <- gam(pm10_2_13_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb14n <- gam(pm10_2_14_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb15n <- gam(pm10_2_15_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb16n <- gam(pm10_2_16_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb17n <- gam(pm10_2_17_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb18n <- gam(pm10_2_18_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb19n <- gam(pm10_2_19_night ~ s(X, Y), data = pm10.winter, method = "ML")
feb20n <- gam(pm10_2_20_night ~ s(X, Y), data = pm10.winter, method = "ML")

###############
#--Create DF--#
###############
feb_mgcv <- data.frame(expand.grid(X = seq(min(pm10.winter$X), max(pm10.winter$X), length=200),
                                   Y = seq(min(pm10.winter$Y), max(pm10.winter$Y), length=200)))


#############
#--Predict--#
#############
feb_mgcv$feb11d<- feb11d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb11n<- feb11n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb12d<- feb12d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb12n<- feb12n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb13d<- feb13d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb13n<- feb13n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb14d<- feb14d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb14n<- feb14n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb15d<- feb15d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb15n<- feb15n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb16d<- feb16d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb16n<- feb16n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb17d<- feb17d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb17n<- feb17n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb18d<- feb18d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb18n<- feb18n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb19d<- feb19d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb19n<- feb19n %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb20d<- feb20d %>% predict(feb_mgcv, type = "response")
feb_mgcv$feb20n<- feb20n %>% predict(feb_mgcv, type = "response")


##-- Find Mean and variance
stat <- feb_mgcv %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% 
  summarise(mean= round(mean(value),1), 
            median= round(median(value),1), 
            sd= round(sd(value),1),
            max = round(max(value),2),
            min = round(min(value),2)) %>% 
  rename(Hour = key)

which( colnames(pm10.winter)=="pm10_2_21_day") # Find first column

mid.feb <- pm10.winter[,c(2:3, 147:166)]
coordinates(mid.feb) <- ~X+Y
proj4string(mid.feb) <- CRS("+init=epsg:5181")

mid.feb.rd <- pm10.win.rd[,c(1:2, 147:166)]
coordinates(mid.feb.rd) <- ~X+Y
proj4string(mid.feb.rd) <- CRS("+init=epsg:5181")


ras.mgcv <- rasterFromXYZ(feb_mgcv) 
crs(ras.mgcv) <- CRS('+init=epsg:5181')

writeRaster(ras.mgcv, filename="../Results/Mapchange/pm10_02_S2_GAM.tif", format="GTiff", overwrite=TRUE)

########
#-RMSE-#
########

obs <- as.data.frame(mid.feb) %>% dplyr::select(X,Y,everything())
pred.df <- data.frame(X = obs$X, Y = obs$Y)
RMSE <- data.frame(X = obs$X, Y = obs$Y)

# for loop
for(i in 1:20){
  pred.df[,i+2] <- extract(ras.mgcv[[i]], mid.feb)
  RMSE[,i+2] <- sqrt(abs(pred.df[,i+2] - obs[,i+2]))^2
}

colnames(RMSE) <- colnames(obs)
RMSE %>% dplyr::select(-c(1:2)) %>% as.matrix() %>% mean()
stat$rmse <- RMSE %>% dplyr::select(-c(1:2)) %>% colMeans() %>% round(digits = 3)

RMSE %>%
  filter_all(all_vars(. > 1)) %>% 
  left_join(stations_df, by = c("X", "Y")) %>% 
  dplyr::select(-c(Road_Dist, DEM)) -> count_RMSE_over1

count_RMSE_over1 %>% dim()

write.csv(count_RMSE_over1, "../Results/RMSE/pm10_GAM_02_S2.csv", row.names = F)


##-- Plotting

GAM.df <- as.data.frame(ras.mgcv, xy = TRUE)  %>% rename(X = "x", Y = "y") %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "pm10") 

GAM.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_contour(aes(x = X, y = Y, z = pm10),bins = 30, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = stat, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_text(data = stat, aes(x = 188900,  y = 469000, label = paste0("RMSE=" , rmse)), size = 3) + 
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> GAM # 1200 x 550 

# Export PNG
ggsave("../Results/Outcome/pm10_GAM_02_S2.png", GAM, width = 11, height = 5, dpi = 100)



####################
#--Export Boxplot--#
####################
GAM.df$Month <- "Feb"
write.csv(GAM.df, "../Results/Boxplot/pm10_02_S2_GAM.csv", row.names = F)
GAM.df <- GAM.df %>% dplyr::select(-Month)


#####################
#--Resample Raster--#
#####################
ras.road <- raster("../Data/road_10km_re.tif")  # Import raster
res.mgcv <- resample(ras.mgcv, ras.road, method = "bilinear") # resample 
res.mgcv <- merge(ras.road, res.mgcv) # merge

# assign road
road_01 = road_02 = road_03 = road_04 = road_05 = 
road_06 = road_07 = road_08 = road_09 = road_10 =
road_11 = road_12 = road_13 = road_14 = road_15 = 
road_16 = road_17 = road_18 = road_19 = road_20 = ras.road
# stack raster and remove individual raster files
road.stack <- stack(road_01, road_02, road_03, road_04, road_05, 
                    road_06, road_07, road_08, road_09, road_10,
                    road_11, road_12, road_13, road_14, road_15, 
                    road_16, road_17, road_18, road_19, road_20
)
rm(road_01, road_02, road_03, road_04, road_05, 
   road_06, road_07, road_08, road_09, road_10,
   road_11, road_12, road_13, road_14, road_15, 
   road_16, road_17, road_18, road_19, road_20
)

# add road ratio values to GAM raster
ratio.mid.feb <- pm10.win.ratio[141:160,]

for(i in 1:20){
  #road.stack[[i]] <- road.stack[[i]] * ratio.pm10.sum$ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 1] <- ratio.mid.feb$Back.Road.Ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 2] <- ratio.mid.feb$Back.High.Ratio[i]
}

# add pm10 and road values
r.poll.rd <- overlay(res.mgcv, road.stack, fun = function(x,y){ifelse(y != 0, x*y, x)})
names(r.poll.rd) <- c("feb11d", "feb11n", "feb12d", "feb12n","feb13d", "feb13n", "feb14d", "feb14n", "feb15d", "feb15n", "feb16d", "feb16n", "feb17d", "feb17n", "feb18d", "feb18n", "feb19d", "feb19n", "feb20d", "feb20n")

writeRaster(r.poll.rd, filename="../Results/Mapchange/pm10_02_S2_GAM_final.tif", format="GTiff", overwrite=TRUE)

#####################
#ras.mgcv.df <- as.data.frame(r.poll.rd, xy = TRUE) # easy way
# however, since we resampled and changed our data
# with different resolution imamges and extent, the easier way doesn't work
ras <- xyFromCell(r.poll.rd, 1:ncell(r.poll.rd))
GAM.rd <- as.data.frame(r.poll.rd) 

##-- Find Mean and variance

ras.krige.stat <- data.frame(ras, GAM.rd)

stat1 <- ras.krige.stat %>% dplyr::select(-c(x,y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
  rename(Hour = key)

#####
ras.GAM.rd <- data.frame(ras, GAM.rd) %>% 
  reshape2::melt(id = c("x", "y"), variable.name = "Hour", value.name = "pm10") 

ras.GAM.rd %>% 
  ggplot() +
  geom_tile(aes(x = x, y = y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_text(data = stat1, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat1, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> final


# Export PNG
ggsave("../Results/Outcome_With_Road/pm10_GAM_final_02_S2.png", final, width = 11, height = 5, dpi = 100)



##############
#--Extract--##
##############
# Attributes of Monitoring stations
back_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Fixed") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

road_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Road") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

# Spatial Points

back <- mid.feb
road <- mid.feb.rd

colnames(back@data) <- c("feb11d", "feb11n", "feb12d", "feb12n","feb13d", "feb13n", "feb14d", "feb14n", "feb15d", "feb15n", "feb16d", "feb16n", "feb17d", "feb17n", "feb18d", "feb18n", "feb19d", "feb19n", "feb20d", "feb20n")

colnames(road@data) <- c("feb11d", "feb11n", "feb12d", "feb12n","feb13d", "feb13n", "feb14d", "feb14n", "feb15d", "feb15n", "feb16d", "feb16n", "feb17d", "feb17n", "feb18d", "feb18n", "feb19d", "feb19n", "feb20d", "feb20n")

# Silim Coordinates: 195793.5 442361.5
# Gwanakgu Office location: 193837.2 442737.9


###########
#--Final--#
###########
back_final <- back_df %>% filter(Station != 131233) %>% cbind(extract(r.poll.rd, back))
back_obs   <- back_df %>% filter(Station != 131233) %>% cbind(back@data)
back_pred  <- back_df %>% filter(Station != 131233) %>% cbind(extract(ras.mgcv, back))
road_final <- road_df %>% cbind(extract(r.poll.rd, road))
road_obs   <- road_df %>% cbind(road@data)
road_pred  <- road_df %>% cbind(extract(ras.mgcv, road))

##########
#--Plot--#
##########

plot_back_pred <- reshape2::melt(back_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_back_final <- reshape2::melt(back_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")

								 
plot_back_obs <- reshape2::melt(back_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_back_fin <- rbind(plot_back_pred, plot_back_final) %>% rbind(plot_back_obs)



##
plot_road_pred <- reshape2::melt(road_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_road_final <- reshape2::melt(road_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")
								 
plot_road_obs <- reshape2::melt(road_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_road_fin <- rbind(plot_road_pred, plot_road_final) %>% rbind(plot_road_obs)




# Export File
write.csv(plot_back_fin, "../Results/Validation/pm10_Feb_S2_GAM.csv",row.names=FALSE)
write.csv(plot_road_fin, "../Results/Validation/pm10_rd_Feb_S2_GAM.csv",row.names=FALSE)
