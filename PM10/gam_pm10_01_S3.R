options(scipen = 100, "rgdal_show_exportToProj4_warnings"="none")
library(tidyverse)
library(sf)
library(mgcv)
library(raster)

load("../Data/pm10.RData")
stations <- read_sf("../Data/stations_10km.shp")
stations_df <- stations %>% filter (F.R == "Fixed")%>% st_set_geometry(NULL)
seoul <- read_sf("../Data/Seoul_City.shp") %>% as_Spatial() %>% fortify()
pm10.winter <- merge(pm10.win.bk, stations_df, by.x = c("Station.ID", "X", "Y"), by.y = c("Station", "X", "Y"))


#JANUARY GAM MODEL
jan21d <- gam(pm10_1_21_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan22d <- gam(pm10_1_22_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan23d <- gam(pm10_1_23_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan24d <- gam(pm10_1_24_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan25d <- gam(pm10_1_25_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan26d <- gam(pm10_1_26_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan27d <- gam(pm10_1_27_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan28d <- gam(pm10_1_28_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan29d <- gam(pm10_1_29_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan30d <- gam(pm10_1_30_day ~ s(X, Y), data = pm10.winter, method = "ML")
jan31d <- gam(pm10_1_31_day ~ s(X, Y), data = pm10.winter, method = "ML")

jan21n <- gam(pm10_1_21_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan22n <- gam(pm10_1_22_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan23n <- gam(pm10_1_23_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan24n <- gam(pm10_1_24_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan25n <- gam(pm10_1_25_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan26n <- gam(pm10_1_26_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan27n <- gam(pm10_1_27_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan28n <- gam(pm10_1_28_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan29n <- gam(pm10_1_29_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan30n <- gam(pm10_1_30_night ~ s(X, Y), data = pm10.winter, method = "ML")
jan31n <- gam(pm10_1_31_night ~ s(X, Y), data = pm10.winter, method = "ML")

###############
#--Create DF--#
###############
jan_mgcv <- data.frame(expand.grid(X = seq(min(pm10.winter$X), max(pm10.winter$X), length=200),
                                   Y = seq(min(pm10.winter$Y), max(pm10.winter$Y), length=200)))

#############
#--Predict--#
#############
jan_mgcv$jan21d<- jan21d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan21n<- jan21n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan22d<- jan22d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan22n<- jan22n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan23d<- jan23d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan23n<- jan23n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan24d<- jan24d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan24n<- jan24n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan25d<- jan25d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan25n<- jan25n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan26d<- jan26d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan26n<- jan26n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan27d<- jan27d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan27n<- jan27n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan28d<- jan28d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan28n<- jan28n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan29d<- jan29d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan29n<- jan29n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan30d<- jan30d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan30n<- jan30n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan31d<- jan31d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan31n<- jan31n %>% predict(jan_mgcv, type = "response")


##-- Find Mean and variance
stat <- jan_mgcv %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% 
  summarise(mean= round(mean(value),1), 
            median= round(median(value),1), 
            sd= round(sd(value),1),
            max = round(max(value),2),
            min = round(min(value),2)) %>% 
  rename(Hour = key)


which( colnames(pm10.winter)=="pm10_1_21_day") # Find first column

bot.jan <- pm10.winter[,c(2:3, 45:66)]
coordinates(bot.jan) <- ~X+Y
proj4string(bot.jan) <- CRS("+init=epsg:5181")

bot.jan.rd <- pm10.win.rd[,c(1:2, 45:66)]
coordinates(bot.jan.rd) <- ~X+Y
proj4string(bot.jan.rd) <- CRS("+init=epsg:5181")


ras.mgcv <- rasterFromXYZ(jan_mgcv) 
crs(ras.mgcv) <- CRS('+init=epsg:5181')

writeRaster(ras.mgcv, filename="../Results/Mapchange/pm10_01_S3_GAM.tif", format="GTiff", overwrite=TRUE)

########
#-RMSE-#
########

obs <- as.data.frame(bot.jan) %>% dplyr::select(X,Y,everything())
pred.df <- data.frame(X = obs$X, Y = obs$Y)
RMSE <- data.frame(X = obs$X, Y = obs$Y)

# for loop
for(i in 1:22){
  pred.df[,i+2] <- extract(ras.mgcv[[i]], bot.jan)
  RMSE[,i+2] <- sqrt(abs(pred.df[,i+2] - obs[,i+2]))^2
}

colnames(RMSE) <- colnames(obs)
RMSE %>% dplyr::select(-c(1:2)) %>% as.matrix() %>% mean()
stat$rmse <- RMSE %>% dplyr::select(-c(1:2)) %>% colMeans() %>% round(digits = 3)

RMSE %>% 
  filter_all(all_vars(. > 1)) %>% 
  left_join(stations_df, by = c("X", "Y")) %>% 
  dplyr::select(-c(Road_Dist, DEM)) -> count_RMSE_over1

count_RMSE_over1 %>% dim()

write.csv(count_RMSE_over1, "../Results/RMSE/pm10_GAM_01_S3.csv", row.names = F)


##-- Plotting

GAM.df <- as.data.frame(ras.mgcv, xy = TRUE)  %>% rename(X = "x", Y = "y") %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "pm10") 

GAM.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_contour(aes(x = X, y = Y, z = pm10),bins = 30, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = stat, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_text(data = stat, aes(x = 188900,  y = 469000, label = paste0("RMSE=" , rmse)), size = 3) + 
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> GAM # 1200 x 550 

# Export PNG
ggsave("../Results/Outcome/pm10_GAM_01_S3.png", GAM, width = 11, height = 5, dpi = 100)


####################
#--Export Boxplot--#
####################
GAM.df$Month <- "Jan"
write.csv(GAM.df, "../Results/Boxplot/pm10_01_S3_GAM.csv", row.names = F)
GAM.df <- GAM.df %>% dplyr::select(-Month)

#####################
#--Resample Raster--#
#####################
ras.road <- raster("../Data/road_10km_re.tif")  # Import raster
res.mgcv <- resample(ras.mgcv, ras.road, method = "bilinear") # resample 
res.mgcv <- merge(ras.road, res.mgcv) # merge

# assign road
road_01 = road_02 = road_03 = road_04 = road_05 = 
  road_06 = road_07 = road_08 = road_09 = road_10 =
  road_11 = road_12 = road_13 = road_14 = road_15 = 
  road_16 = road_17 = road_18 = road_19 = road_20 = road_21 = road_22 =ras.road
# stack raster and remove individual raster files
road.stack <- stack(road_01, road_02, road_03, road_04, road_05, 
                    road_06, road_07, road_08, road_09, road_10,
                    road_11, road_12, road_13, road_14, road_15, 
                    road_16, road_17, road_18, road_19, road_20, road_21, road_22
)
rm(road_01, road_02, road_03, road_04, road_05, 
   road_06, road_07, road_08, road_09, road_10,
   road_11, road_12, road_13, road_14, road_15, 
   road_16, road_17, road_18, road_19, road_20, road_21, road_22
)

# add road ratio values to GAM raster
ratio.bot.jan <- pm10.win.ratio[99:120,]

for(i in 1:22){
  #road.stack[[i]] <- road.stack[[i]] * ratio.pm10.sum$ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 1] <- ratio.bot.jan$Back.Road.Ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 2] <- ratio.bot.jan$Back.High.Ratio[i]
}

# add pm10 and road values
r.poll.rd <- overlay(res.mgcv, road.stack, fun = function(x,y){ifelse(y != 0, x*y, x)})
names(r.poll.rd) <- c('jan21d', 'jan21n','jan22d', 'jan22n','jan23d', 'jan23n','jan24d', 'jan24n', 'jan25d', 'jan25n', 'jan26d', 'jan26n', 'jan27d', 'jan27n','jan28d', 'jan28n','jan29d', 'jan29n', 'jan30d', 'jan30n', 'jan31d', 'jan31n')

writeRaster(r.poll.rd, filename="../Results/Mapchange/pm10_01_S3_GAM_final.tif", format="GTiff", overwrite=TRUE)

#####################
#ras.mgcv.df <- as.data.frame(r.poll.rd, xy = TRUE) # easy way
# however, since we resampled and changed our data
# with different resolution imamges and extent, the easier way doesn't work
ras <- xyFromCell(r.poll.rd, 1:ncell(r.poll.rd))
GAM.rd <- as.data.frame(r.poll.rd) 

##-- Find Mean and variance

ras.krige.stat <- data.frame(ras, GAM.rd)

stat1 <- ras.krige.stat %>% dplyr::select(-c(x,y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
  rename(Hour = key)

#####
ras.GAM.rd <- data.frame(ras, GAM.rd) %>% 
  reshape2::melt(id = c("x", "y"), variable.name = "Hour", value.name = "pm10") 

ras.GAM.rd %>% 
  ggplot() +
  geom_tile(aes(x = x, y = y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_text(data = stat1, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat1, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> final


# Export PNG
ggsave("../Results/Outcome_With_Road/pm10_GAM_final_01_S3.png", final, width = 11, height = 5, dpi = 100)


##############
#--Extract--##
##############
# Attributes of Monitoring stations
back_df <- stations %>% 
  st_set_geometry(NULL) %>%
  filter(`F.R` == "Fixed") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

road_df <- stations %>% 
  st_set_geometry(NULL) %>%
  filter(`F.R` == "Road") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

# Spatial Points
back <- bot.jan
road <- bot.jan.rd

colnames(back@data) <- c('jan21d', 'jan21n','jan22d', 'jan22n','jan23d', 'jan23n','jan24d', 'jan24n', 'jan25d', 'jan25n', 'jan26d', 'jan26n', 'jan27d', 'jan27n','jan28d', 'jan28n','jan29d', 'jan29n', 'jan30d', 'jan30n', 'jan31d', 'jan31n')

colnames(road@data) <- c('jan21d', 'jan21n','jan22d', 'jan22n','jan23d', 'jan23n','jan24d', 'jan24n', 'jan25d', 'jan25n', 'jan26d', 'jan26n', 'jan27d', 'jan27n','jan28d', 'jan28n','jan29d', 'jan29n', 'jan30d', 'jan30n', 'jan31d', 'jan31n')

# Silim Coordinates: 195793.5 442361.5
# Gwanakgu Office location: 193837.2 442737.9


###########
#--Final--#
###########
back_final <- back_df %>% filter(Station != 131233) %>% cbind(extract(r.poll.rd, back))
back_obs   <- back_df %>% filter(Station != 131233) %>% cbind(back@data)
back_pred  <- back_df %>% filter(Station != 131233) %>% cbind(extract(ras.mgcv, back))
road_final <- road_df %>% cbind(extract(r.poll.rd, road))
road_obs   <- road_df %>% cbind(road@data)
road_pred  <- road_df %>% cbind(extract(ras.mgcv, road))

##########
#--Plot--#
##########

plot_back_pred <- reshape2::melt(back_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_back_final <- reshape2::melt(back_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")

								 
plot_back_obs <- reshape2::melt(back_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_back_fin <- rbind(plot_back_pred, plot_back_final) %>% rbind(plot_back_obs)



##
plot_road_pred <- reshape2::melt(road_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_road_final <- reshape2::melt(road_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")
								 
plot_road_obs <- reshape2::melt(road_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_road_fin <- rbind(plot_road_pred, plot_road_final) %>% rbind(plot_road_obs)




# Export File
write.csv(plot_back_fin, "../Results/Validation/pm10_Jan_S3_GAM.csv",row.names=FALSE)
write.csv(plot_road_fin, "../Results/Validation/pm10_rd_Jan_S3_GAM.csv",row.names=FALSE)
