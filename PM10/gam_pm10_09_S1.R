options(scipen = 100, "rgdal_show_exportToProj4_warnings"="none")
library(tidyverse)
library(sf)
library(mgcv)
library(raster)

load("../Data/pm10.RData")
stations <- read_sf("../Data/stations_10km.shp")
stations_df <- stations %>% filter (F.R == "Fixed")%>% st_set_geometry(NULL)
seoul <- read_sf("../Data/Seoul_City.shp") %>% as('Spatial') %>% fortify()
pm10.summer <- merge(pm10.sum.bk, stations_df, by.x = c("Station.ID", "X", "Y"), by.y = c("Station", "X", "Y"))


# SEPTEMBER 2013
sep01d <- gam(pm10_9_01_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep02d <- gam(pm10_9_02_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep03d <- gam(pm10_9_03_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep04d <- gam(pm10_9_04_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep05d <- gam(pm10_9_05_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep06d <- gam(pm10_9_06_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep07d <- gam(pm10_9_07_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep08d <- gam(pm10_9_08_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep09d <- gam(pm10_9_09_day ~ s(X, Y), data = pm10.summer, method = "ML")
sep10d <- gam(pm10_9_10_day ~ s(X, Y), data = pm10.summer, method = "ML")

sep01n <- gam(pm10_9_01_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep02n <- gam(pm10_9_02_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep03n <- gam(pm10_9_03_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep04n <- gam(pm10_9_04_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep05n <- gam(pm10_9_05_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep06n <- gam(pm10_9_06_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep07n <- gam(pm10_9_07_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep08n <- gam(pm10_9_08_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep09n <- gam(pm10_9_09_night ~ s(X, Y), data = pm10.summer, method = "ML")
sep10n <- gam(pm10_9_10_night ~ s(X, Y), data = pm10.summer, method = "ML")


###############
#--Create DF--#
###############
sep_mgcv <- data.frame(expand.grid(X = seq(min(pm10.summer$X), max(pm10.summer$X), length=200),
                                   Y = seq(min(pm10.summer$Y), max(pm10.summer$Y), length=200)))


#############
#--Predict--#
#############
sep_mgcv$sep01d<- sep01d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep01n<- sep01n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep02d<- sep02d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep02n<- sep02n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep03d<- sep03d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep03n<- sep03n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep04d<- sep04d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep04n<- sep04n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep05d<- sep05d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep05n<- sep05n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep06d<- sep06d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep06n<- sep06n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep07d<- sep07d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep07n<- sep07n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep08d<- sep08d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep08n<- sep08n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep09d<- sep09d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep09n<- sep09n %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep10d<- sep10d %>% predict(sep_mgcv, type = "response")
sep_mgcv$sep10n<- sep10n %>% predict(sep_mgcv, type = "response")


##-- Find Mean and variance
stat <- sep_mgcv %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% 
  summarise(mean= round(mean(value),1), 
            median= round(median(value),1), 
            sd= round(sd(value),1),
            max = round(max(value),2),
            min = round(min(value),2)) %>% 
  rename(Hour = key)


which( colnames(pm10.summer)=="pm10_9_11_day") # Find first column

top.sep <- pm10.summer[,c(2:3, 67:86)]
coordinates(top.sep) <- ~X+Y
proj4string(top.sep) <- CRS("+init=epsg:5181")

top.sep.rd <- pm10.sum.rd[,c(1:2, 67:86)]
coordinates(top.sep.rd) <- ~X+Y
proj4string(top.sep.rd) <- CRS("+init=epsg:5181")


ras.mgcv <- rasterFromXYZ(sep_mgcv) 
crs(ras.mgcv) <- CRS('+init=epsg:5181')

###################
#--Export Raster--#
###################
writeRaster(ras.mgcv, filename="../Results/Mapchange/pm10_09_S1_GAM.tif", format="GTiff", overwrite=TRUE)

########
#-RMSE-#
########

obs <- as.data.frame(top.sep) %>% dplyr::select(X,Y,everything())
pred.df <- data.frame(X = obs$X, Y = obs$Y)
RMSE <- data.frame(X = obs$X, Y = obs$Y)

# for loop
for(i in 1:20){
  pred.df[,i+2] <- extract(ras.mgcv[[i]], top.sep)
  RMSE[,i+2] <- sqrt(abs(pred.df[,i+2] - obs[,i+2]))^2
}

colnames(RMSE) <- colnames(obs)
RMSE %>% dplyr::select(-c(1:2)) %>% as.matrix() %>% mean()
stat$rmse <- RMSE %>% dplyr::select(-c(1:2)) %>% colMeans() %>% round(digits = 3)

RMSE %>% 
  filter_all(all_vars(. > 1)) %>% 
  left_join(stations_df, by = c("X", "Y")) %>% 
  dplyr::select(-c(Road_Dist, DEM)) -> count_RMSE_over1

count_RMSE_over1 %>% dim()

write.csv(count_RMSE_over1, "../Results/RMSE/pm10_GAM_09_S1.csv", row.names = F)


##-- Plotting

GAM.df <- as.data.frame(ras.mgcv, xy = TRUE)  %>% rename(X = "x", Y = "y") %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "pm10") 

GAM.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_contour(aes(x = X, y = Y, z = pm10),bins = 30, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = stat, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_text(data = stat, aes(x = 188900,  y = 469000, label = paste0("RMSE=" , rmse)), size = 3) + 
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> GAM # 1200 x 550 

# Export PNG
ggsave("../Results/Outcome/pm10_GAM_09_S1.png", GAM, width = 11, height = 5, dpi = 100)


####################
#--Export Boxplot--#
####################
GAM.df$Month <- "Sep"
write.csv(GAM.df, "../Results/Boxplot/pm10_09_S1_GAM.csv", row.names = F)
GAM.df <- GAM.df %>% dplyr::select(-Month)


#####################
#--Resample Raster--#
#####################
ras.road <- raster("../Data/road_10km_re.tif")  # Import raster
res.mgcv <- resample(ras.mgcv, ras.road, method = "bilinear") # resample 
res.mgcv <- merge(ras.road, res.mgcv) # merge

# assign road
road_01 = road_02 = road_03 = road_04 = road_05 = 
  road_06 = road_07 = road_08 = road_09 = road_10 =
  road_11 = road_12 = road_13 = road_14 = road_15 = 
  road_16 = road_17 = road_18 = road_19 = road_20 = ras.road
# stack raster and remove individual raster files
road.stack <- stack(road_01, road_02, road_03, road_04, road_05, 
                    road_06, road_07, road_08, road_09, road_10,
                    road_11, road_12, road_13, road_14, road_15, 
                    road_16, road_17, road_18, road_19, road_20
)
rm(road_01, road_02, road_03, road_04, road_05, 
   road_06, road_07, road_08, road_09, road_10,
   road_11, road_12, road_13, road_14, road_15, 
   road_16, road_17, road_18, road_19, road_20
)

# add road ratio values to GAM raster
ratio.top.sep <- pm10.sum.ratio[63:82,]

for(i in 1:20){
  #road.stack[[i]] <- road.stack[[i]] * pm10.sum.ratio$ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 1] <- ratio.top.sep$Back.Road.Ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 2] <- ratio.top.sep$Back.High.Ratio[i]
}

# add pm10 and road values
r.poll.rd <- overlay(res.mgcv, road.stack, fun = function(x,y){ifelse(y != 0, x*y, x)})
names(r.poll.rd) <- c("sep01d", "sep01n", "sep02d", "sep02n","sep03d", "sep03n", "sep04d", "sep04n", "sep05d", "sep05n", "sep06d", "sep06n", "sep07d", "sep07n", "sep08d", "sep08n", "sep09d", "sep09n", "sep10d", "sep10n")

writeRaster(r.poll.rd, filename="../Results/Mapchange/pm10_09_S1_GAM_final.tif", format="GTiff", overwrite=TRUE)

#####################
#ras.mgcv.df <- as.data.frame(r.poll.rd, xy = TRUE) # easy way
# however, since we resampled and changed our data
# with different resolution imamges and extent, the easier way doesn't work
ras <- xyFromCell(r.poll.rd, 1:ncell(r.poll.rd))
GAM.rd <- as.data.frame(r.poll.rd) 

##-- Find Mean and variance

ras.krige.stat <- data.frame(ras, GAM.rd)

stat1 <- ras.krige.stat %>% dplyr::select(-c(x,y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
  rename(Hour = key)

#####
ras.GAM.rd <- data.frame(ras, GAM.rd) %>% 
  reshape2::melt(id = c("x", "y"), variable.name = "Hour", value.name = "pm10") 

ras.GAM.rd %>% 
  ggplot() +
  geom_tile(aes(x = x, y = y, fill = pm10)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_text(data = stat1, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat1, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> final


# Export PNG
ggsave("../Results/Outcome_With_Road/pm10_GAM_final_09_S1.png", final, width = 11, height = 5, dpi = 100)




##############
#--Extract--##
##############
# Attributes of Monitoring stations
back_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Fixed") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

road_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Road") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

# Spatial Points
#back <- top.jan[c(2,8,17,18,42,48),] #'Jongno-gu', 'Seongbuk-gu', 'Gwanak-gu', 'Gangnam-gu', 'Daeya', 'Onam'
back <- top.sep
road <- top.sep.rd

colnames(back@data) <- c("sep01d", "sep01n", "sep02d", "sep02n","sep03d", "sep03n", "sep04d", "sep04n", "sep05d", "sep05n", "sep06d", "sep06n", "sep07d", "sep07n", "sep08d", "sep08n", "sep09d", "sep09n", "sep10d", "sep10n")

colnames(road@data) <- c("sep01d", "sep01n", "sep02d", "sep02n","sep03d", "sep03n", "sep04d", "sep04n", "sep05d", "sep05n", "sep06d", "sep06n", "sep07d", "sep07n", "sep08d", "sep08n", "sep09d", "sep09n", "sep10d", "sep10n")


# Silim Coordinates: 195793.5 442361.5
# Gwanakgu Office location: 193837.2 442737.9

###########
#--Final--#
###########
back_final <- back_df %>% filter(Station != 131233) %>% cbind(extract(r.poll.rd, back))
back_obs   <- back_df %>% filter(Station != 131233) %>% cbind(back@data)
back_pred  <- back_df %>% filter(Station != 131233) %>% cbind(extract(ras.mgcv, back))
road_final <- road_df %>% cbind(extract(r.poll.rd, road))
road_obs   <- road_df %>% cbind(road@data)
road_pred  <- road_df %>% cbind(extract(ras.mgcv, road))

##########
#--Plot--#
##########

plot_back_pred <- reshape2::melt(back_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_back_final <- reshape2::melt(back_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")

								 
plot_back_obs <- reshape2::melt(back_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_back_fin <- rbind(plot_back_pred, plot_back_final) %>% rbind(plot_back_obs)



##
plot_road_pred <- reshape2::melt(road_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_road_final <- reshape2::melt(road_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")
								 
plot_road_obs <- reshape2::melt(road_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_road_fin <- rbind(plot_road_pred, plot_road_final) %>% rbind(plot_road_obs)




# Export File
write.csv(plot_back_fin, "../Results/Validation/pm10_Sep_S1_GAM.csv",row.names=FALSE)
write.csv(plot_road_fin, "../Results/Validation/pm10_rd_Sep_S1_GAM.csv",row.names=FALSE)
