options(scipen = 100)
library(tidyverse)
library(sf)
library(mgcv)
library(raster)

load("Data/no2.RData")
stations <- read_sf("Data/stations_10km.shp")
stations_df <- stations %>% filter (F.R == "Fixed")%>% st_set_geometry(NULL)
seoul <- read_sf("Data/Seoul_City.shp") %>% as_Spatial() %>% fortify()
no2.winter <- merge(no2.win.bk, stations_df, by.x = c("Station.ID", "X", "Y"), by.y = c("Station", "X", "Y"))



#JANUARY GAM MODEL
jan11d <- gam(no2_1_11_day ~ s(X, Y), data = no2.winter, method = "ML")
jan12d <- gam(no2_1_12_day ~ s(X, Y), data = no2.winter, method = "ML")
jan13d <- gam(no2_1_13_day ~ s(X, Y), data = no2.winter, method = "ML")
jan14d <- gam(no2_1_14_day ~ s(X, Y), data = no2.winter, method = "ML")
jan15d <- gam(no2_1_15_day ~ s(X, Y), data = no2.winter, method = "ML")
jan16d <- gam(no2_1_16_day ~ s(X, Y), data = no2.winter, method = "ML")
jan17d <- gam(no2_1_17_day ~ s(X, Y), data = no2.winter, method = "ML")
jan18d <- gam(no2_1_18_day ~ s(X, Y), data = no2.winter, method = "ML")
jan19d <- gam(no2_1_19_day ~ s(X, Y), data = no2.winter, method = "ML")
jan20d <- gam(no2_1_20_day ~ s(X, Y), data = no2.winter, method = "ML")

jan11n <- gam(no2_1_11_night ~ s(X, Y), data = no2.winter, method = "ML")
jan12n <- gam(no2_1_12_night ~ s(X, Y), data = no2.winter, method = "ML")
jan13n <- gam(no2_1_13_night ~ s(X, Y), data = no2.winter, method = "ML")
jan14n <- gam(no2_1_14_night ~ s(X, Y), data = no2.winter, method = "ML")
jan15n <- gam(no2_1_15_night ~ s(X, Y), data = no2.winter, method = "ML")
jan16n <- gam(no2_1_16_night ~ s(X, Y), data = no2.winter, method = "ML")
jan17n <- gam(no2_1_17_night ~ s(X, Y), data = no2.winter, method = "ML")
jan18n <- gam(no2_1_18_night ~ s(X, Y), data = no2.winter, method = "ML")
jan19n <- gam(no2_1_19_night ~ s(X, Y), data = no2.winter, method = "ML")
jan20n <- gam(no2_1_20_night ~ s(X, Y), data = no2.winter, method = "ML")

###############
#--Create DF--#
###############
jan_mgcv <- data.frame(expand.grid(X = seq(min(no2.winter$X), max(no2.winter$X), length=200),
                                   Y = seq(min(no2.winter$Y), max(no2.winter$Y), length=200)))

#############
#--Predict--#
#############
jan_mgcv$jan11d<- jan11d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan11n<- jan11n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan12d<- jan12d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan12n<- jan12n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan13d<- jan13d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan13n<- jan13n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan14d<- jan14d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan14n<- jan14n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan15d<- jan15d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan15n<- jan15n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan16d<- jan16d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan16n<- jan16n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan17d<- jan17d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan17n<- jan17n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan18d<- jan18d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan18n<- jan18n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan19d<- jan19d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan19n<- jan19n %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan20d<- jan20d %>% predict(jan_mgcv, type = "response")
jan_mgcv$jan20n<- jan20n %>% predict(jan_mgcv, type = "response")

##-- Find Mean and variance
stat <- jan_mgcv %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% 
  summarise(mean= round(mean(value),1), 
            median= round(median(value),1), 
            sd= round(sd(value),1),
            max = round(max(value),2),
            min = round(min(value),2)) %>% 
  rename(Hour = key)


which( colnames(no2.winter)=="no2_1_11_day") # Find first column

mid.jan <- no2.winter[,c(2:3, 25:44)]
coordinates(mid.jan) <- ~X+Y
proj4string(mid.jan) <- CRS("+init=epsg:5181")


mid.jan.rd <- no2.win.rd[,c(1:2, 25:44)]
coordinates(mid.jan.rd) <- ~X+Y
proj4string(mid.jan.rd) <- CRS("+init=epsg:5181")


ras.mgcv <- rasterFromXYZ(jan_mgcv) 
crs(ras.mgcv) <- CRS('+init=epsg:5181')

writeRaster(ras.mgcv, filename="Results/Mapchange/no2_01_S2_GAM.tif", format="GTiff", overwrite=TRUE)

########
#-RMSE-#
########

obs <- as.data.frame(mid.jan) %>% dplyr::select(X,Y,everything())
pred.df <- data.frame(X = obs$X, Y = obs$Y)
RMSE <- data.frame(X = obs$X, Y = obs$Y)

# for loop
for(i in 1:20){
  pred.df[,i+2] <- extract(ras.mgcv[[i]], mid.jan)
  RMSE[,i+2] <- sqrt(abs(pred.df[,i+2] - obs[,i+2]))^2
}

colnames(RMSE) <- colnames(obs)
RMSE %>% dplyr::select(-c(1:2)) %>% as.matrix() %>% mean()
stat$rmse <- RMSE %>% dplyr::select(-c(1:2)) %>% colMeans() %>% round(digits = 3)

RMSE %>% 
  filter_all(all_vars(. > 1)) %>% 
  left_join(stations_df, by = c("X", "Y")) %>% 
  dplyr::select(-c(Road_Dist, DEM)) -> count_RMSE_over1

count_RMSE_over1 %>% dim()

write.csv(count_RMSE_over1, "Results/RMSE/NO2_GAM_01_S2.csv", row.names = F)

##-- Plotting

GAM.df <- as.data.frame(ras.mgcv, xy = TRUE)  %>% rename(X = "x", Y = "y") %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "NO2") 

GAM.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = NO2)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_contour(aes(x = X, y = Y, z = NO2),bins = 30, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = stat, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_text(data = stat, aes(x = 188900,  y = 469000, label = paste0("RMSE=" , rmse)), size = 3) + 
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> GAM # 1200 x 550 

# Export PNG
ggsave("Results/Outcome/NO2_GAM_01_S2.png", GAM, width = 11, height = 5, dpi = 100)



####################
#--Export Boxplot--#
####################
GAM.df$Month <- "Jan"
write.csv(GAM.df, "Results/Boxplot/no2_01_S2_GAM.csv", row.names = F)
GAM.df <- GAM.df %>% dplyr::select(-Month)

#####################
#--Resample Raster--#
#####################
ras.road <- raster("Data/road_10km_re.tif")  # Import raster
res.mgcv <- resample(ras.mgcv, ras.road, method = "bilinear") # resample 
res.mgcv <- merge(ras.road, res.mgcv) # merge

# assign road
road_01 = road_02 = road_03 = road_04 = road_05 = 
  road_06 = road_07 = road_08 = road_09 = road_10 =
  road_11 = road_12 = road_13 = road_14 = road_15 = 
  road_16 = road_17 = road_18 = road_19 = road_20 = ras.road
# stack raster and remove individual raster files
road.stack <- stack(road_01, road_02, road_03, road_04, road_05, 
                    road_06, road_07, road_08, road_09, road_10,
                    road_11, road_12, road_13, road_14, road_15, 
                    road_16, road_17, road_18, road_19, road_20
)
rm(road_01, road_02, road_03, road_04, road_05, 
   road_06, road_07, road_08, road_09, road_10,
   road_11, road_12, road_13, road_14, road_15, 
   road_16, road_17, road_18, road_19, road_20
)

# add road ratio values to GAM raster
ratio.mid.jan <- no2.win.ratio[79:98,]

for(i in 1:20){
  #road.stack[[i]] <- road.stack[[i]] * ratio.no2.sum$ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 1] <- ratio.mid.jan$Back.Road.Ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 2] <- ratio.mid.jan$Back.High.Ratio[i]
}

# add no2 and road values
r.poll.rd <- overlay(res.mgcv, road.stack, fun = function(x,y){ifelse(y != 0, x*y, x)})
names(r.poll.rd) <- c('jan11d', 'jan11n','jan12d', 'jan12n','jan13d', 'jan13n','jan14d', 'jan14n', 'jan15d', 'jan15n', 'jan16d', 'jan16n', 'jan17d', 'jan17n','jan18d', 'jan18n','jan19d', 'jan19n', 'jan20d', 'jan20n')

writeRaster(r.poll.rd, filename="Results/Mapchange/no2_01_S2_GAM_final.tif", format="GTiff", overwrite=TRUE)

#####################
#ras.mgcv.df <- as.data.frame(r.poll.rd, xy = TRUE) # easy way
# however, since we resampled and changed our data
# with different resolution imamges and extent, the easier way doesn't work
ras <- xyFromCell(r.poll.rd, 1:ncell(r.poll.rd))
GAM.rd <- as.data.frame(r.poll.rd) 

##-- Find Mean and variance

ras.krige.stat <- data.frame(ras, GAM.rd)

stat1 <- ras.krige.stat %>% dplyr::select(-c(x,y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
  rename(Hour = key)

#####
ras.GAM.rd <- data.frame(ras, GAM.rd) %>% 
  reshape2::melt(id = c("x", "y"), variable.name = "Hour", value.name = "NO2") 

ras.GAM.rd %>% 
  ggplot() +
  geom_tile(aes(x = x, y = y, fill = NO2)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_text(data = stat1, aes(x = 187000,  y = 434000, label = paste0("mean = " , mean)), size = 3) + 
  geom_text(data = stat1, aes(x = 184000,  y = 430500, label = paste0("sd = " , sd)), size = 3) + 
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> final


# Export PNG
ggsave("Results/Outcome_With_Road/NO2_GAM_final_01_S2.png", final, width = 11, height = 5, dpi = 100)


##############
#--Extract--##
##############
# Attributes of Monitoring stations
back_df <- stations %>% 
  st_set_geometry(NULL) %>%
  filter(`F.R` == "Fixed") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

road_df <- stations %>% 
  st_set_geometry(NULL) %>%
  filter(`F.R` == "Road") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

# Spatial Points
back <- mid.jan
road <- mid.jan.rd

colnames(back@data) <- c("jan11d", "jan11n", "jan12d", "jan12n","jan13d", "jan13n", "jan14d", "jan14n", "jan15d", "jan15n", "jan16d", "jan16n", "jan17d", "jan17n", "jan18d", "jan18n", "jan19d", "jan19n", "jan20d", "jan20n")

colnames(road@data) <- c("jan11d", "jan11n", "jan12d", "jan12n","jan13d", "jan13n", "jan14d", "jan14n", "jan15d", "jan15n", "jan16d", "jan16n", "jan17d", "jan17n", "jan18d", "jan18n", "jan19d", "jan19n", "jan20d", "jan20n")

# Silim Coordinates: 195793.5 442361.5
# Gwanakgu Office location: 193837.2 442737.9


###########
#--Final--#
###########
back_final <- back_df %>% filter(Station != 131233) %>% cbind(extract(r.poll.rd, back))
back_obs   <- back_df %>% filter(Station != 131233) %>% cbind(back@data)
back_pred  <- back_df %>% filter(Station != 131233) %>% cbind(extract(ras.mgcv, back))
road_final <- road_df %>% cbind(extract(r.poll.rd, road))
road_obs   <- road_df %>% cbind(road@data)
road_pred  <- road_df %>% cbind(extract(ras.mgcv, road))

##########
#--Plot--#back@bbox
##########

plot_back_pred <- reshape2::melt(back_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_back_final <- reshape2::melt(back_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")

								 
plot_back_obs <- reshape2::melt(back_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_back_fin <- rbind(plot_back_pred, plot_back_final) %>% rbind(plot_back_obs)



##
plot_road_pred <- reshape2::melt(road_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_road_final <- reshape2::melt(road_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")
								 
plot_road_obs <- reshape2::melt(road_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_road_fin <- rbind(plot_road_pred, plot_road_final) %>% rbind(plot_road_obs)




# Export File
write.csv(plot_back_fin, "Results/Validation/no2_Jan_S2_GAM.csv",row.names=FALSE)
write.csv(plot_road_fin, "Results/Validation/no2_rd_Jan_S2_GAM.csv",row.names=FALSE)
