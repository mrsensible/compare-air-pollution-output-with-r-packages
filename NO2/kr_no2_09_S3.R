options(scipen = 100, "rgdal_show_exportToProj4_warnings"="none")
library(tidyverse)
library(sf)
library(raster)
library(rgdal)
library(gstat)
library(moments)

load("../Data/no2.RData")
stations <- read_sf("../Data/stations_10km.shp")
stations_df <- stations %>% filter (F.R == "Fixed")%>% st_set_geometry(NULL)
seoul <- read_sf("../Data/Seoul_City.shp") %>% as('Spatial') %>% fortify()

no2.summer <- merge(no2.sum.bk, stations_df, by.x = c("Station.ID", "X", "Y"), by.y = c("Station", "X", "Y"))
coordinates(no2.summer) <- ~X+Y
proj4string(no2.summer) <- CRS("+init=epsg:5181")

bot.sep <- no2.summer[105:124]


sk <- data.frame(Date = bot.sep@data %>% 
                   reshape2::melt(id.vars = NULL, variable.name = "Date", value.name = "pm10") %>% 
                   dplyr::select(1) %>% unique(),
                 mean = colMeans(bot.sep@data[1:20]) %>% round(2),
                 median = apply(bot.sep@data[1:20], 2, FUN = median) %>% round(2),
                 skewness = skewness(bot.sep@data[1:20]) %>% round(2),
                 kurtosis = kurtosis(bot.sep@data[1:20]) %>% round(2))


bot.sep@data %>% 
  reshape2::melt(id.vars = NULL, variable.name = "Date", value.name = "pm10") %>% 
  ggplot(aes(x= pm10, fill= Date)) +
  geom_histogram(binwidth=10, colour = "black")+
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("mean = " , mean)), hjust = -0.05, vjust = 1.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("median = " , median)), hjust = -0.05, vjust = 2.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("skewness = " , skewness)), hjust = -0.05, vjust = 3.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("kurtosis = " , kurtosis)), hjust = -0.05, vjust = 4.1, size = 3.5) +
  facet_wrap(~Date) +
  theme_bw() +
  theme(legend.position = "none",
        strip.text.x = element_text(size = 15)) -> hist_poll


ggsave("../Results/Hist/no2_hist_09_S3.png", hist_poll, width = 12, height = 10)#, scale = 1.5)


##########
options(warn = -1) # don't print warnings
myVario <- list()
myList <- list()


for(i in 1:20){
  myVario[[length(myVario)+1]] <- variogram(log(bot.sep[[i]]) ~ 1, bot.sep, cutoff = 30000, width = 3000)
  myList[[length(myList) + 1]]  <- fit.variogram(myVario[[i]], 
                                                 vgm(psill = 0.2,
                                                                      nugget= 0.05,
                                                     model = "Ste"),
                                                 fit.kappa = TRUE, fit.method = 6)
  
}


myList[[3]]  <- fit.variogram(myVario[[3]], 
                              vgm(psill = .7,
                                  range = 45000,
                                  nugget= .05,
                                  model="Ste",
                                  kappa = 10),
                              fit.kappa = TRUE, fit.method = 6)


myList[[4]]  <- fit.variogram(myVario[[4]], 
                              vgm(psill = .7,
                                  range = 45000,
                                  nugget= .05,
                                  model="Ste",
                                  kappa = 10),
                              fit.kappa = TRUE, fit.method = 6)

myList[[6]]  <- fit.variogram(myVario[[6]], 
                              vgm(psill = .3,
                                  range = 45000,
                                  nugget= .03,
                                  model="Ste",
                                  kappa = 10),
                              fit.kappa = TRUE, fit.method = 6)

myList[[7]]  <- fit.variogram(myVario[[7]], 
                              vgm(psill = .04,
                                  range = 45000,
                                  nugget= .02,
                                  model = "Ste",
                                  kappa = 10))

myList[[8]]  <- fit.variogram(myVario[[8]], 
                              vgm(psill = .04,
                                  range = 45000,
                                  nugget= .02,
                                  model = "Ste",
                                  kappa = 10),
                              fit.kappa = TRUE, fit.method = 6)

myList[[10]]  <- fit.variogram(myVario[[10]], 
                              vgm(psill = .06,
                                  range = 45000,
                                  nugget= .04,
                                  model = "Ste",
                                  kappa = 10))


myList[[13]]  <- fit.variogram(myVario[[13]], 
                              vgm(psill = .002,
                                  range = 45000,
                                  nugget= .03,
                                  model = "Sph",
                                  kappa = 10))


myList[[14]]  <- fit.variogram(myVario[[14]], 
                               vgm(psill = .1,
                                   range = 45000,
                                   nugget= .01,
                                   model = "Ste",
                                   kappa = 10),
                               fit.kappa = TRUE, fit.method = 6)

myList[[18]]  <- fit.variogram(myVario[[18]], 
                               vgm(psill = .1,
                                   range = 45000,
                                   nugget= .02,
                                   model = "Ste",
                                   kappa = 10))


library(gridExtra)

p01 <- plot(myVario[[1]], myList[[1]], main = "Sep 21st\nDay hours")
p02 <- plot(myVario[[2]], myList[[2]], main = "Sep 21st\nNight hours")
p03 <- plot(myVario[[3]], myList[[3]], main = "Sep 22nd\nDay hours")
p04 <- plot(myVario[[4]], myList[[4]], main = "Sep 22nd\nNight hours")
p05 <- plot(myVario[[5]], myList[[5]], main = "Sep 23rd\nDay hours")
p06 <- plot(myVario[[6]], myList[[6]], main = "Sep 23rd\nNight hours")
p07 <- plot(myVario[[7]], myList[[7]], main = "Sep 24th\nDay hours")
p08 <- plot(myVario[[8]], myList[[8]], main = "Sep 24th\nNight hours")
p09 <- plot(myVario[[9]], myList[[9]], main = "Sep 25th\nDay hours")
p10 <- plot(myVario[[10]], myList[[10]], main = "Sep 25th\nNight hours")
p11 <- plot(myVario[[11]], myList[[11]], main = "Sep 26th\nDay hours")
p12 <- plot(myVario[[12]], myList[[12]], main = "Sep 26th\nNight hours")
p13 <- plot(myVario[[13]], myList[[13]], main = "Sep 27th\nDay hours")
p14 <- plot(myVario[[14]], myList[[14]], main = "Sep 27th\nNight hours")
p15 <- plot(myVario[[15]], myList[[15]], main = "Sep 28th\nDay hours")
p16 <- plot(myVario[[16]], myList[[16]], main = "Sep 28th\nNight hours")
p17 <- plot(myVario[[17]], myList[[17]], main = "Sep 29th\nDay hours")
p18 <- plot(myVario[[18]], myList[[18]], main = "Sep 29th\nNight hours")
p19 <- plot(myVario[[19]], myList[[19]], main = "Sep 30th\nDay hours")
p20 <- plot(myVario[[20]], myList[[20]], main = "Sep 30th\nNight hours")

varplot <- grid.arrange(p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, 
                        p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, 
                        ncol = 6)

ggsave("../Results/Semivariogram/no2_semvario_09_S3.png",varplot, width = 12, height = 10)#, scale = 1.5)

### Data Frame
seoul_grid <- data.frame(expand.grid(X = seq(min(no2.summer$X), max(no2.summer$X), length=200),
                                     Y = seq(min(no2.summer$Y), max(no2.summer$Y), length=200)))
coordinates(seoul_grid) <- ~X+Y
proj4string(seoul_grid) <- CRS("+init=epsg:5181")


##############
#--Kriging--##
##############
pred.model <- seoul_grid@coords
var.model <- seoul_grid@coords

for(i in 1:20) {
  kriging_new <- krige(bot.sep@data[,i]~ X + Y,
                       bot.sep, 
                       seoul_grid,
                       #nmin = 15,
                       #nmax = 30,
                       model = myList[[i]])
  kriging_new$var_model <- data.frame(kriging_new$var1.var)
  var.model <- cbind(var.model, kriging_new$var_model)
  xyz <- as.data.frame(kriging_new$var1.pred)
  colnames(xyz) <- colnames(bot.sep@data)[i]
  pred.model <- cbind(pred.model, xyz)
} 

##-- Add ColNames
colnames(pred.model) <- c("X", "Y", "sep21d", "sep21n", "sep22d", "sep22n","sep23d", "sep23n", "sep24d", "sep24n", "sep25d", "sep25n", "sep26d", "sep26n", "sep27d", "sep27n", "sep28d", "sep28n", "sep29d", "sep29n", "sep30d", "sep30n")

colnames(var.model) <- c("X", "Y", "sep21d", "sep21n", "sep22d", "sep22n","sep23d", "sep23n", "sep24d", "sep24n", "sep25d", "sep25n", "sep26d", "sep26n", "sep27d", "sep27n", "sep28d", "sep28n", "sep29d", "sep29n", "sep30d", "sep30n")


##-- Find Mean and variance
stat <- pred.model %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), median = round(median(value),1), 
                              sd= round(sd(value),1), max = max(value),min = min(value)) %>% rename(Hour = key)

statvar <- var.model %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), median = round(median(value),1), 
                              sd= round(sd(value),1), max = max(value),min = min(value)) %>% rename(Hour = key)


########
# RMSE #
########
pred <- pred.model
r.pred <- rasterFromXYZ(pred)
crs(r.pred) <- CRS('+init=epsg:5181')

obs <- as.data.frame(bot.sep) %>% dplyr::select(X,Y,everything())
pred.df <- data.frame(X = obs$X, Y = obs$Y)
RMSE <- data.frame(X = obs$X, Y = obs$Y)

# for loop
for(i in 1:20){
  pred.df[,i+2] <- extract(r.pred[[i]], bot.sep)
  RMSE[,i+2] <- sqrt(abs(pred.df[,i+2] - obs[,i+2]))^2
  
}

colnames(RMSE) <- colnames(obs)
RMSE %>% dplyr::select(-c(1:2)) %>% as.matrix() %>% mean()
stat$rmse <- RMSE %>% dplyr::select(-c(1:2)) %>% colMeans() %>% round(digits = 3)

RMSE %>% 
  filter_all(all_vars(. > 1)) %>% 
  left_join(stations_df, by = c("X", "Y")) %>% 
  dplyr::select(-c(Road_Dist, DEM)) -> count_RMSE_over1

count_RMSE_over1 %>% dim()

write.csv(count_RMSE_over1, "../Results/RMSE/no2_kr_09_S3.csv", row.names = F)


##-- Plotting

ras.krige.df <- pred.model %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "NO2") 

ras.krige.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = NO2)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_contour(aes(x = X, y = Y, z = NO2),bins = 20, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = stat, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) +
  geom_text(data = stat, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) +
  geom_text(data = stat, aes(-Inf, Inf, label = paste0("RMSE=" , rmse)), hjust = -.1, vjust = 1.2, size = 3.5) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> kriged # 1200 x 550 

# Export PNG
ggsave("../Results/Outcome/NO2_kriged_pred_09_S3.png", kriged, width = 11, height = 5, dpi = 100)


ras.var.df <- var.model %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "NO2") 

ras.var.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = NO2)) +
  scale_fill_distiller(palette = "BrBG", na.value = NA) + #, limits = c(0,12), breaks = c(0,3,6,9,12)) +
  geom_contour(aes(x = X, y = Y, z = NO2),bins = 20, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = statvar, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) +
  geom_text(data = statvar, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> kriged_stv 

# Export PNG
ggsave("../Results/Outcome/NO2_kriged_var_09_S3.png", kriged_stv, width = 11, height = 5, dpi = 100)


####################
#--Export Boxplot--#
####################
ras.krige.df$Month <- "Sep"
write.csv(ras.krige.df, "../Results/Boxplot/no2_09_S3_kr.csv", row.names = F)
ras.krige.df <- ras.krige.df %>% dplyr::select(-Month)


######################
#--Cross Validation--#
######################
CV <- list()
CV.RMSE <- vector()
CV.R2 <- vector()

for(k in 1:20){
  CV[[length(CV)+1]] <- krige.cv(get(colnames(bot.sep@data[k]))~ 1, 
                                 no2.summer, 
                                 myList[[k]], # Semi-Variogram
                                 nmax = 30,
                                 nfold=10)
  
  RMSE <- sqrt(mean((CV[[k]]$observed - CV[[k]]$residual)^2)) #RMSE, Ideally small
  CV.RMSE <- append(CV.RMSE,RMSE)
  R2 <- (cor(CV[[k]]$observed, CV[[k]]$residual))^2
  CV.R2 <- append(CV.R2,R2)
}

CV.RMSE
CV.R2



library(gridExtra)

CV1 <- bubble(CV[[1]], "residual", main = "10-fold CV residuals\nFeb11 Day 2014")
CV2 <- bubble(CV[[2]], "residual", main = "10-fold CV residuals\nFeb11 Night 2014")
CV3 <- bubble(CV[[3]], "residual", main = "10-fold CV residuals\nFeb12 Day 2014")
CV4 <- bubble(CV[[4]], "residual", main = "10-fold CV residuals\nFeb12 Night 2014")

grid.arrange(CV1, CV2, CV3, CV4)



# convert to Raster Bricks
krige <- rasterFromXYZ(pred.model, 
                       crs="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
                       digits=5)

###################
#--Export Raster--#
###################
writeRaster(krige, filename="../Results/Mapchange/no2_09_S3_kr.tif", format="GTiff", overwrite=TRUE)


ras.road <- raster("../Data/road_10km_re.tif")  # Import raster
res.mgcv <- resample(krige, ras.road, method = "bilinear") # resample 
res.mgcv <- merge(ras.road, res.mgcv) # merge

# assign road
road_01 = road_02 = road_03 = road_04 = road_05 = 
  road_06 = road_07 = road_08 = road_09 = road_10 =
  road_11 = road_12 = road_13 = road_14 = road_15 = 
  road_16 = road_17 = road_18 = road_19 = road_20 = ras.road

# stack raster and remove individual raster files
road.stack <- stack(road_01, road_02, road_03, road_04, road_05, 
                    road_06, road_07, road_08, road_09, road_10,
                    road_11, road_12, road_13, road_14, road_15, 
                    road_16, road_17, road_18, road_19, road_20
)
rm(road_01, road_02, road_03, road_04, road_05, 
   road_06, road_07, road_08, road_09, road_10,
   road_11, road_12, road_13, road_14, road_15, 
   road_16, road_17, road_18, road_19, road_20
)

# add road ratio values to GAM raster
ratio.bot.sep <- no2.sum.ratio[117:136,]

for(i in 1:20){
  #road.stack[[i]] <- road.stack[[i]] * no2.sum.ratio$ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 1] <- ratio.bot.sep$Back.Road.Ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 2] <- ratio.bot.sep$Back.High.Ratio[i]
}

# add no2 and road values
r.poll.rd <- overlay(res.mgcv, road.stack, fun = function(x,y){ifelse(y != 0, x*y, x)})
names(r.poll.rd) <- c("sep21d", "sep21n", "sep22d", "sep22n","sep23d", "sep23n", "sep24d", "sep24n", "sep25d", "sep25n", "sep26d", "sep26n", "sep27d", "sep27n", "sep28d", "sep28n", "sep29d", "sep29n", "sep30d", "sep30n")

writeRaster(r.poll.rd, filename="../Results/Mapchange/no2_09_S3_kr_final.tif", format="GTiff", overwrite=TRUE)

#####################
#ras.mgcv.df <- as.data.frame(r.poll.rd, xy = TRUE) # easy way
# however, since we resampled and changed our data
# with different resolution imamges and extent, the easier way doesn't work
ras <- xyFromCell(r.poll.rd, 1:ncell(r.poll.rd))
krige.df <- as.data.frame(r.poll.rd) 

##-- Find Mean and variance

ras.krige.stat <- data.frame(ras, krige.df)

stat1 <- ras.krige.stat %>% dplyr::select(-c(x,y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
  rename(Hour = key)

#####
ras.krige.df <- data.frame(ras, krige.df) %>% 
  reshape2::melt(id = c("x", "y"), variable.name = "Hour", value.name = "NO2") 

ras.krige.df %>% 
  ggplot() +
  geom_tile(aes(x = x, y = y, fill = NO2)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_text(data = stat1, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) + 
  geom_text(data = stat1, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) + 
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> final # 1200 x 550 


# Export PNG
ggsave("../Results/Outcome_With_Road/NO2_kriged_final_09_S3.png", final, width = 11, height = 5, dpi = 100)



# Attributes of Monitoring stations
back_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Fixed") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

road_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Road") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

# Spatial Points
#back <- top.jan[c(2,8,17,18,42,48),] #'Jongno-gu', 'Seongbuk-gu', 'Gwanak-gu', 'Gangnam-gu', 'Daeya', 'Onam'
back <- bot.sep

bot.sep.rd <- no2.sum.rd[,c(1:2, 107:126)]
coordinates(bot.sep.rd) <- ~X+Y
proj4string(bot.sep.rd) <- CRS("+init=epsg:5181")

road <- bot.sep.rd

colnames(back@data) <- c("sep21d", "sep21n", "sep22d", "sep22n","sep23d", "sep23n", "sep24d", "sep24n", "sep25d", "sep25n", "sep26d", "sep26n", "sep27d", "sep27n", "sep28d", "sep28n", "sep29d", "sep29n", "sep30d", "sep30n")

colnames(road@data) <- c("sep21d", "sep21n", "sep22d", "sep22n","sep23d", "sep23n", "sep24d", "sep24n", "sep25d", "sep25n", "sep26d", "sep26n", "sep27d", "sep27n", "sep28d", "sep28n", "sep29d", "sep29n", "sep30d", "sep30n")


# Silim Coordinates: 195793.5 442361.5
# Gwanakgu Office location: 193837.2 442737.9

###########
#--Final--#
###########
back_final <- back_df %>% filter(Station != 131233) %>% cbind(extract(r.poll.rd, back))
back_obs   <- back_df %>% filter(Station != 131233) %>% cbind(back@data)
back_pred  <- back_df %>% filter(Station != 131233) %>% cbind(extract(krige, back))
road_final <- road_df %>% cbind(extract(r.poll.rd, road))
road_obs   <- road_df %>% cbind(road@data)
road_pred  <- road_df %>% cbind(extract(krige, road))

##########
#--Plot--#
##########

plot_back_pred <- reshape2::melt(back_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_back_final <- reshape2::melt(back_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")

								 
plot_back_obs <- reshape2::melt(back_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_back_fin <- rbind(plot_back_pred, plot_back_final) %>% rbind(plot_back_obs)



##
plot_road_pred <- reshape2::melt(road_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_road_final <- reshape2::melt(road_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")
								 
plot_road_obs <- reshape2::melt(road_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_road_fin <- rbind(plot_road_pred, plot_road_final) %>% rbind(plot_road_obs)



# Export File
write.csv(plot_back_fin, "../Results/Validation/no2_Sep_S3_kr.csv",row.names=FALSE)
write.csv(plot_road_fin, "../Results/Validation/no2_rd_Sep_S3_kr.csv",row.names=FALSE)
