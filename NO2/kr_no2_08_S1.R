options(scipen = 100, "rgdal_show_exportToProj4_warnings"="none")
library(tidyverse)
library(sf)
library(raster)
library(rgdal)
library(gstat)
library(moments)

load("../Data/no2.RData")
stations <- read_sf("../Data/stations_10km.shp")
stations_df <- stations %>% filter (F.R == "Fixed")%>% st_set_geometry(NULL)
seoul <- read_sf("../Data/Seoul_City.shp") %>% as('Spatial') %>% fortify()

#################################
##-Merge Files for Summer 2013-##
#################################

no2.summer <- merge(no2.sum.bk, stations_df, by.x = c("Station.ID", "X", "Y"), by.y = c("Station", "X", "Y"))
coordinates(no2.summer) <- ~X+Y
proj4string(no2.summer) <- CRS("+init=epsg:5181")

top.aug <- no2.summer[3:22]

sk <- data.frame(Date = top.aug@data %>% 
                   reshape2::melt(id.vars = NULL, variable.name = "Date", value.name = "pm10") %>% 
                   dplyr::select(1) %>% unique(),
                 mean = colMeans(top.aug@data[1:20]) %>% round(2),
                 median = apply(top.aug@data[1:20], 2, FUN = median) %>% round(2),
                 skewness = skewness(top.aug@data[1:20]) %>% round(2),
                 kurtosis = kurtosis(top.aug@data[1:20]) %>% round(2))


top.aug@data %>% 
  reshape2::melt(id.vars = NULL, variable.name = "Date", value.name = "pm10") %>% 
  ggplot(aes(x= pm10, fill= Date)) +
  geom_histogram(binwidth=10, colour = "black")+
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("mean = " , mean)), hjust = -0.05, vjust = 1.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("median = " , median)), hjust = -0.05, vjust = 2.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("skewness = " , skewness)), hjust = -0.05, vjust = 3.1, size = 3.5) +
  geom_text(data = sk, aes(-Inf, Inf, label = paste0("kurtosis = " , kurtosis)), hjust = -0.05, vjust = 4.1, size = 3.5) +
  facet_wrap(~Date) +
  theme_bw() +
  theme(legend.position = "none",
        strip.text.x = element_text(size = 15)) -> hist_poll


ggsave("../Results/Hist/no2_hist_08_S1.png", hist_poll, width = 12, height = 10)#, scale = 1.5)


##########
options(warn = -1) # don't print warnings
myVario <- list()
myList <- list()

for(i in 1:20){
  myVario[[length(myVario)+1]] <- variogram(log(top.aug[[i]]) ~ 1, top.aug, cutoff = 30000, width = 3000)
  myList[[length(myList) + 1]]  <- fit.variogram(myVario[[i]], 
                                                 vgm(psill = 0.2,
                                                     nugget= 0.05,
                                                     model = "Ste"),
                                                 fit.kappa = TRUE, fit.method = 6)
}

myList[[2]]  <- fit.variogram(myVario[[2]], 
                              vgm(psill = .021,
                                  nugget= .04,
                                  model = "Ste",
                                  kappa = 0.05),
                              fit.kappa = TRUE)


myList[[5]]  <- fit.variogram(myVario[[5]], 
                              vgm(psill = .021,
                                  nugget= .04,
                                  model = "Ste",
                                  kappa = 0.05),
                              fit.kappa = TRUE)


########################
#--Plot Semivariogram--#
########################

library(gridExtra)

p01 <- plot(myVario[[1]],  myList[[1]],  main = "Aug 01st\nDay hours")
p02 <- plot(myVario[[2]],  myList[[2]],  main = "Aug 01st\nNight hours")
p03 <- plot(myVario[[3]],  myList[[3]],  main = "Aug 02nd\nDay hours")
p04 <- plot(myVario[[4]],  myList[[4]],  main = "Aug 02nd\nNight hours")
p05 <- plot(myVario[[5]],  myList[[5]],  main = "Aug 03rd\nDay hours")
p06 <- plot(myVario[[6]],  myList[[6]],  main = "Aug 03rd\nNight hours")
p07 <- plot(myVario[[7]],  myList[[7]],  main = "Aug 04th\nDay hours")
p08 <- plot(myVario[[8]],  myList[[8]],  main = "Aug 04th\nNight hours")
p09 <- plot(myVario[[9]],  myList[[9]],  main = "Aug 05th\nDay hours")
p10 <- plot(myVario[[10]], myList[[10]], main = "Aug 05th\nNight hours")
p11 <- plot(myVario[[11]], myList[[11]], main = "Aug 06th\nDay hours")
p12 <- plot(myVario[[12]], myList[[12]], main = "Aug 06th\nNight hours")
p13 <- plot(myVario[[13]], myList[[13]], main = "Aug 07th\nDay hours")
p14 <- plot(myVario[[14]], myList[[14]], main = "Aug 07th\nNight hours")
p15 <- plot(myVario[[15]], myList[[15]], main = "Aug 08th\nDay hours")
p16 <- plot(myVario[[16]], myList[[16]], main = "Aug 08th\nNight hours")
p17 <- plot(myVario[[17]], myList[[17]], main = "Aug 09th\nDay hours")
p18 <- plot(myVario[[18]], myList[[18]], main = "Aug 09th\nNight hours")
p19 <- plot(myVario[[19]], myList[[19]], main = "Aug 10th\nDay hours")
p20 <- plot(myVario[[20]], myList[[20]], main = "Aug 10th\nNight hours")


varplot <- grid.arrange(p01, p02, p03, p04, p05, p06, p07, p08, p09, p10, 
                        p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, 
                        ncol = 6)

ggsave("../Results/Semivariogram/no2_semvario_08_S1.png",varplot, width = 12, height = 10)#, scale = 1.5)


### Data Frame
seoul_grid <- data.frame(expand.grid(X = seq(min(no2.summer$X), max(no2.summer$X), length=200),
                                     Y = seq(min(no2.summer$Y), max(no2.summer$Y), length=200)))
coordinates(seoul_grid) <- ~X+Y
proj4string(seoul_grid) <- CRS("+init=epsg:5181")

#https://gis.stackexchange.com/questions/157279/saving-results-in-automap-r-package-for-time-series-data

##############
#--Kriging--##
##############
pred.model <- seoul_grid@coords
var.model <- seoul_grid@coords

for(i in 1:20) {
  kriging_new <- krige(top.aug@data[,i]~ X + Y,
                       top.aug, 
                       seoul_grid,
                       model = myList[[i]])
  kriging_new$var_model <- data.frame(kriging_new$var1.var)
  var.model <- cbind(var.model, kriging_new$var_model)
  xyz <- as.data.frame(kriging_new$var1.pred)
  colnames(xyz) <- colnames(top.aug@data)[i]
  pred.model <- cbind(pred.model, xyz)
} 


##-- Add ColNames
colnames(pred.model) <- c("X", "Y", "aug01d", "aug01n", "aug02d", "aug02n","aug03d", "aug03n", "aug04d", "aug04n", "aug05d", "aug05n", "aug06d", "aug06n", "aug07d", "aug07n", "aug08d", "aug08n", "aug09d", "aug09n", "aug10d", "aug10n")

colnames(var.model) <- c("X", "Y", "aug01d", "aug01n", "aug02d", "aug02n","aug03d", "aug03n", "aug04d", "aug04n", "aug05d", "aug05n", "aug06d", "aug06n", "aug07d", "aug07n", "aug08d", "aug08n", "aug09d", "aug09n", "aug10d", "aug10n")


##-- Find Mean and variance
stat <- pred.model %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), median = round(median(value),1), 
                              sd= round(sd(value),1), max = max(value),min = min(value)) %>% rename(Hour = key)

statvar <- var.model %>% dplyr::select(-c(X,Y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), median = round(median(value),1), 
                              sd= round(sd(value),1), max = max(value),min = min(value)) %>% rename(Hour = key)



########
# RMSE #
########
pred <- pred.model
r.pred <- rasterFromXYZ(pred)
crs(r.pred) <- CRS('+init=epsg:5181')

obs <- as.data.frame(top.aug) %>% dplyr::select(X,Y,everything())
pred.df <- data.frame(X = obs$X, Y = obs$Y)
RMSE <- data.frame(X = obs$X, Y = obs$Y)

# for loop
for(i in 1:20){
  pred.df[,i+2] <- extract(r.pred[[i]], top.aug)
  RMSE[,i+2] <- sqrt(abs(pred.df[,i+2] - obs[,i+2]))^2
  
}

colnames(RMSE) <- colnames(obs)
RMSE %>% dplyr::select(-c(1:2)) %>% as.matrix() %>% mean()
stat$rmse <- RMSE %>% dplyr::select(-c(1:2)) %>% colMeans() %>% round(digits = 3)

RMSE %>% 
  filter_all(all_vars(. > 1)) %>% 
  left_join(stations_df, by = c("X", "Y")) %>% 
  dplyr::select(-c(Road_Dist, DEM)) -> count_RMSE_over1

count_RMSE_over1 %>% dim()

write.csv(count_RMSE_over1, "../Results/RMSE/no2_kr_08_S1.csv", row.names = F)



##-- Plotting

ras.krige.df <- pred.model %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "NO2") 

ras.krige.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = NO2)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_contour(aes(x = X, y = Y, z = NO2),bins = 20, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = stat, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) +
  geom_text(data = stat, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) +
  geom_text(data = stat, aes(-Inf, Inf, label = paste0("RMSE=" , rmse)), hjust = -.1, vjust = 1.2, size = 3.5) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> kriged # 1200 x 550 

# Export PNG
ggsave("../Results/Outcome/NO2_kriged_pred_08_S1.png", kriged, width = 11, height = 5, dpi = 100)


ras.var.df <- var.model %>% 
  reshape2::melt(id = c("X", "Y"), variable.name = "Hour", value.name = "NO2") 

ras.var.df %>% 
  ggplot() +
  geom_tile(aes(x = X, y = Y, fill = NO2)) +
  scale_fill_distiller(palette = "BrBG", na.value = NA) + #, limits = c(0,12), breaks = c(0,3,6,9,12)) +
  geom_contour(aes(x = X, y = Y, z = NO2),bins = 20, colour = "grey40", alpha = 0.7) +
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  geom_text(data = statvar, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) +
  geom_text(data = statvar, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> kriged_stv 

# Export PNG
ggsave("../Results/Outcome/NO2_kriged_var_08_S1.png", kriged_stv, width = 11, height = 5, dpi = 100)


####################
#--Export Boxplot--#
####################
ras.krige.df$Month <- "Aug"
write.csv(ras.krige.df, "../Results/Boxplot/no2_08_S1_kr.csv", row.names = F)
ras.krige.df <- ras.krige.df %>% dplyr::select(-Month)


# convert to Raster Bricks
krige <- rasterFromXYZ(pred.model, 
                       crs="+proj=tmerc +lat_0=38 +lon_0=127 +k=1 +x_0=200000 +y_0=500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
                       digits=5)

###################
#--Export Raster--#
###################
library(raster)
writeRaster(krige, filename="../Results/Mapchange/no2_08_S1_kr.tif", format="GTiff", overwrite=TRUE)

ras.road <- raster("../Data/road_10km_re.tif")  # Import raster
res.mgcv <- resample(krige, ras.road, method = "bilinear") # resample 
res.mgcv <- merge(ras.road, res.mgcv) # merge

# assign road
road_01 = road_02 = road_03 = road_04 = road_05 = 
  road_06 = road_07 = road_08 = road_09 = road_10 =
  road_11 = road_12 = road_13 = road_14 = road_15 = 
  road_16 = road_17 = road_18 = road_19 = road_20 = ras.road

# stack raster and remove individual raster files
road.stack <- stack(road_01, road_02, road_03, road_04, road_05, 
                    road_06, road_07, road_08, road_09, road_10,
                    road_11, road_12, road_13, road_14, road_15, 
                    road_16, road_17, road_18, road_19, road_20
)
rm(road_01, road_02, road_03, road_04, road_05, 
   road_06, road_07, road_08, road_09, road_10,
   road_11, road_12, road_13, road_14, road_15, 
   road_16, road_17, road_18, road_19, road_20
)

# add road ratio values to GAM raster
ratio.top.aug <- no2.sum.ratio[1:20,]


for(i in 1:20){
  #road.stack[[i]] <- road.stack[[i]] * no2.sum.ratio$ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 1] <- ratio.top.aug$Back.Road.Ratio[i]
  values(road.stack)[values(road.stack[[i]]) == 2] <- ratio.top.aug$Back.High.Ratio[i]
}

# add no2 and road values
r.poll.rd <- overlay(res.mgcv, road.stack, fun = function(x,y){ifelse(y != 0, x*y, x)})
names(r.poll.rd) <- c("aug01d", "aug01n", "aug02d", "aug02n","aug03d", "aug03n", "aug04d", "aug04n", "aug05d", "aug05n", "aug06d", "aug06n", "aug07d", "aug07n", "aug08d", "aug08n", "aug09d", "aug09n", "aug10d", "aug10n")

writeRaster(r.poll.rd, filename="../Results/Mapchange/no2_08_S1_kr_final.tif", format="GTiff", overwrite=TRUE)

#####################
#ras.mgcv.df <- as.data.frame(r.poll.rd, xy = TRUE) # easy way
# however, since we resampled and changed our data
# with different resolution imamges and extent, the easier way doesn't work
ras <- xyFromCell(r.poll.rd, 1:ncell(r.poll.rd))
krige.df <- as.data.frame(r.poll.rd) 

##-- Find Mean and variance

ras.krige.stat <- data.frame(ras, krige.df)

stat1 <- ras.krige.stat %>% dplyr::select(-c(x,y)) %>% 
  gather(factor_key = T) %>% 
  group_by(key) %>% summarise(mean= round(mean(value),1), sd= round(sd(value),1), max = max(value),min = min(value)) %>% 
  rename(Hour = key)

#####
ras.krige.df <- data.frame(ras, krige.df) %>% 
  reshape2::melt(id = c("x", "y"), variable.name = "Hour", value.name = "NO2") 

ras.krige.df %>% 
  ggplot() +
  geom_tile(aes(x = x, y = y, fill = NO2)) +
  scale_fill_distiller(palette = "Spectral", na.value = NA, limits = c(0,150), breaks = c(0,25,50,75,100,125,150)) +
  geom_text(data = stat1, aes(-Inf, -Inf, label = paste0("mean = " , mean)), hjust = -.1, vjust = -2, size = 3.5) + 
  geom_text(data = stat1, aes(-Inf, -Inf, label = paste0("sd = " , sd)), hjust = -.1, vjust = -1, size = 3.5) + 
  geom_path(data = seoul, aes(x = long, y = lat), color = 'black', size = 1) +
  facet_wrap(~ Hour, ncol = 8) +
  theme_bw() +
  theme(axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_blank(),
        axis.title.y=element_blank(),
        strip.text.x = element_text(size = 20),
        legend.title=element_text(size=15), 
        legend.text=element_text(size=15)                                  
  ) -> final # 1200 x 550 


# Export PNG
ggsave("../Results/Outcome_With_Road/NO2_kriged_final_08_S1.png", final, width = 11, height = 5, dpi = 100)



# Attributes of Monitoring stations
back_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Fixed") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

road_df <- stations %>% 
  st_set_geometry(NULL) %>% 
  filter(`F.R` == "Road") %>% 
  dplyr::select(-c(Long, Lat, `F.R`, Road_Dist, DEM))

# Spatial Points
back <- top.aug

top.aug.rd <- no2.sum.rd[,c(1:2, 5:24)]
coordinates(top.aug.rd) <- ~X+Y
proj4string(top.aug.rd) <- CRS("+init=epsg:5181")

road <- top.aug.rd

colnames(back@data) <- c("aug01d", "aug01n", "aug02d", "aug02n","aug03d", "aug03n", "aug04d", "aug04n", "aug05d", "aug05n", "aug06d", "aug06n", "aug07d", "aug07n", "aug08d", "aug08n", "aug09d", "aug09n", "aug10d", "aug10n")

colnames(road@data) <- c("aug01d", "aug01n", "aug02d", "aug02n","aug03d", "aug03n", "aug04d", "aug04n", "aug05d", "aug05n", "aug06d", "aug06n", "aug07d", "aug07n", "aug08d", "aug08n", "aug09d", "aug09n", "aug10d", "aug10n")

# Silim Coordinates: 195793.5 442361.5
# Gwanakgu Office location: 193837.2 442737.9


###########
#--Final--#
###########
back_final <- back_df %>% filter(Station != 131233) %>% cbind(extract(r.poll.rd, back))
back_obs   <- back_df %>% filter(Station != 131233) %>% cbind(back@data)
back_pred  <- back_df %>% filter(Station != 131233) %>% cbind(extract(krige, back))
road_final <- road_df %>% cbind(extract(r.poll.rd, road))
road_obs   <- road_df %>% cbind(road@data)
road_pred  <- road_df %>% cbind(extract(krige, road))

##########
#--Plot--#
##########

plot_back_pred <- reshape2::melt(back_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_back_final <- reshape2::melt(back_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")

								 
plot_back_obs <- reshape2::melt(back_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_back_fin <- rbind(plot_back_pred, plot_back_final) %>% rbind(plot_back_obs)



##
plot_road_pred <- reshape2::melt(road_pred, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "pred")

plot_road_final <- reshape2::melt(road_final, 
                                 id = c("X","Y","Station","Name", "Province", "City"), 
                                 variable.name = "Type", 
                                 value.name = "Value") %>% 
								 mutate(Group = "final")
								 
plot_road_obs <- reshape2::melt(road_obs, 
                                id = c("X","Y","Station","Name", "Province", "City"), 
                                variable.name = "Type", 
                                value.name = "Value")%>% 
								mutate(Group = "obs")

plot_road_fin <- rbind(plot_road_pred, plot_road_final) %>% rbind(plot_road_obs)




# Export File
write.csv(plot_back_fin, "../Results/Validation/no2_Aug_S1_kr.csv",row.names=FALSE)
write.csv(plot_road_fin, "../Results/Validation/no2_rd_Aug_S1_kr.csv",row.names=FALSE)

