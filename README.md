# Compare Air Pollution Output with R Packages

This repository compares Spatial Interpolation methods to predict Seoul's air pollution.
There are 76 monitoring stations of which 25 are background stations in Seoul, 32 are background stations that are within the 10km distance from the administrati